<?php

class Database {

	public static function getDbConnection() {
		$servername = "localhost";
		$username = "root";
		$password = "";
		$db = "node_db";
		try {
		   
		    $conn = mysqli_connect($servername, $username, $password, $db);
		} catch(exception $e) {
		    echo "Connection failed: " . $e->getMessage();
		}
		
		return $conn;
	}

	// get all nodes and display it with indention
	// param $parent_id
	public static function getAllNodes($parent_id) {

		$con = self::getDbConnection();
	 	$sql = "SELECT * FROM nodes WHERE parent_id='" .$parent_id . "'";
        $result = mysqli_query($con, $sql);
	   	$total_space = 0;
	   	$ctr=0;
        $div = "";
        while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) 
		{
			$total_space = $row['spaces'] + 4;	
			$spaces = '&nbsp';
			
			for ($i=0; $i < $total_space; $i++) { 
				$spaces .= '&nbsp';
			}		

			$div .="<div id = '".$row['node_id']."'>";

			$div .= "$spaces node ";

			
			$div .= self::getAllNodes($row['node_id']); //call  
			$div .= "</div>";
	    }
	    return $div;
	}

	// Get the last row of node to create new node id
	public static function getLastRow() {
		$node_id = 0;
		$con = self::getDbConnection();
	 	$sql = "SELECT * FROM nodes ORDER BY id DESC LIMIT 1;";
        $result = mysqli_query($con, $sql);

        while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) 
		{
			$node_id = $row['node_id'] + 1;
		}

        return $node_id;
	}
}

?>