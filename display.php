<?php

include_once('Controller.php');

$controller = new Controller;

?>
<!DOCTYPE html>
<html>
<head>
	<title>Display Nodes</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<?php $controller->displayNodes(); ?>
	<br/>
	<a href='index.html' class = "nav-link">Go To Index Page</a> <a href="Controller.php?mode=reset" class = "nav-link">Reset all Nodes</a>
</body>
</html>