<?php
include_once('Database.php');

class Controller {

	//display node on  a seperate page
	public function displayNodes() {
		$database = new Database();
		$div ="<div id = '0'>";
	   	$div .= "root ";
	   	$nodes = $database->getAllNodes(0);
		$div .= $nodes."</div>";

		echo $div;
	}

	//add node using javascript xmlhtpp request
	// param $id, $parent_id, $spaces
	public function addNode($id, $parent_id, $spaces) {

		$database = new Database();
		$con = $database->getDbConnection();

		$last_row = $database::getLastRow();
		if(!empty($last_row)) {
			$id = $last_row;
		} else {
			$id = 1;
		}

	 	$sql = "INSERT into nodes(node_id,parent_id,spaces)
                   values ('".$id."','".$parent_id."','".$spaces."')";
        mysqli_query($con, $sql);

		$response_data = json_encode(["id"=>$id, "parent_id"=>$id]);

		echo $response_data;
			
	}

	//remove a specific node
	// param $id
	public function removeNode($id) {
		$database = new Database();
		$con = $database->getDbConnection();
		$sql = "DELETE FROM nodes WHERE node_id=$id";
		mysqli_query($con, $sql);
	}

	// reset table node
	public function resetNodes() {
		$database = new Database();
		$con = $database->getDbConnection();
		$sql = "TRUNCATE TABLE nodes";
		mysqli_query($con, $sql);

		header("Location: index.html");
		exit();
	}
}

$mode= $id = $space = $reset = null;
$controller = new Controller;

if(!empty($_POST)){
	$mode = $_POST['mode'];
	$id = $_POST['id'];
	$spaces = $_POST['spaces'];
}

if(!empty($_GET['mode'])){
	$mode =  $_GET['mode'];
}

switch ($mode) {
	case 'add':
			$parent_id = $_POST['parent_id'];
			$controller->addNode($id, $parent_id,$spaces);	
		break;

	case 'remove':
			$controller->removeNode($id);	
		break;
	case 'reset':
			$controller->resetNodes();	
		break;
	default:
		# code...
		break;
}